﻿import flash.display.MovieClip;
import flash.display.Loader;
import flash.net.URLRequest;
import flash.display.SimpleButton;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.display.BitmapData;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;

const name1:String = "Forest Chillout";
const name2:String = "Pink Hope";
const name3:String = "Trojan Horse";
const name4:String = "Underwater Chest";
const name5:String = "Aztec Hour";

var stack:int = 0;
var l1:Loader;
var l2:Loader;
var l3:Loader;
var l4:Loader;
var l5:Loader;
var m_th1:MovieClip;
var m_th2:MovieClip;
var m_th3:MovieClip;
var m_th4:MovieClip;
var m_th5:MovieClip;
var m_cover:MovieClip;
var audio:SoundChannel;
var audioVolume:Number = 0.5;
var audioX:int = m_pos.x;
const audioWidth:int = 72;
var audioHeight:int = 0;

function updateVolume() {
	audio.soundTransform = new SoundTransform(audioVolume);	
}

function updatePos() {
	m_pos.x = audioX + audioWidth * audioVolume;	
}

function onPlus(e:MouseEvent) {
	if (audioVolume < 0.9) {
		audioVolume += 0.1;
		updatePos();
		if (audio) updateVolume();
	}
}

function onMinus(e:MouseEvent) {
	if (audioVolume > 0.1) {
		audioVolume -= 0.1;
		updatePos();
		if (audio) updateVolume();
	}
}

function onEnter(e:Event) {
	m_left.height = audioHeight * audio.leftPeak;
	m_right.height = audioHeight * audio.rightPeak;
}

function onSound(e:Event) {
	if (audio) {
		this.removeEventListener(Event.ENTER_FRAME, onEnter);
		audio.stop();
	}
	audio = (e.target as Sound).play(0, 99);
	updateVolume();
	this.addEventListener(Event.ENTER_FRAME, onEnter);
}

function loadSound(soundPath:String) {
	var sound:Sound = new Sound();
	sound.addEventListener(Event.COMPLETE, onSound);
	sound.load(new URLRequest(soundPath));
}

function fillCover(loader:Loader):void {
	var bitmapData:BitmapData = Bitmap(loader.content).bitmapData;
	m_cover.graphics.beginBitmapFill(bitmapData);
	m_cover.graphics.drawRect(0, 0, bitmapData.width, bitmapData.height);
	m_cover.graphics.endFill();
	m_cover.width = m_inside.width;
	m_cover.height = m_inside.height;
}

function onClick(e:MouseEvent):void {
	switch (e.target.name) {
		case (b_th1.name):
			fillCover(l1);
			loadSound("mp3/1.mp3");
			m_title.text = name1;
			break;
		case (b_th2.name):
			fillCover(l2);
			loadSound("mp3/2.mp3");
			m_title.text = name2;
			break;
		case (b_th3.name):
			fillCover(l3);
			loadSound("mp3/3.mp3");
			m_title.text = name3;
			break;
		case (b_th4.name):
			fillCover(l4);
			loadSound("mp3/4.mp3");
			m_title.text = name4;
			break;
		case (b_th5.name):
			fillCover(l5);
			loadSound("mp3/5.mp3");
			m_title.text = name5;
			break;
	}
}

function createThumb(button:SimpleButton, img:Loader):MovieClip {
	//Bitmap
	var thumb:MovieClip = new MovieClip();
	thumb.addChild(img);
	//Image
	img.width = button.width;
	img.height = button.height;
	img.mouseEnabled = false;
	thumb.addChild(img);
	//Thumb
	thumb.x = button.x;
	thumb.y = button.y;
	thumb.mouseEnabled = false;
	this.addChild(thumb);
	//Button
	button.addEventListener(MouseEvent.CLICK, onClick);
	return thumb;
}

function onComplete(e:Event):void {
	stack++;
	if (stack > 4) {
		m_th1 = createThumb(b_th1, l1);
		m_th2 = createThumb(b_th2, l2);
		m_th3 = createThumb(b_th3, l3);
		m_th4 = createThumb(b_th4, l4);
		m_th5 = createThumb(b_th5, l5);
	}
}

function loadImage(imgPath:String):Loader {
	var loader:Loader = new Loader();
	loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
	loader.load(new URLRequest(imgPath));
	return loader;
}

function initCover():MovieClip {
	var cover:MovieClip = new MovieClip();
	cover.x = m_inside.x;
	cover.y = m_inside.y;
	this.addChild(cover);
	return cover;
}

function init():void {
	audioHeight = m_left.height;
	updatePos();
	m_cover = initCover();
	l1 = loadImage("img/1.jpg");
	l2 = loadImage("img/2.jpg");
	l3 = loadImage("img/3.jpg");
	l4 = loadImage("img/4.jpg");
	l5 = loadImage("img/5.jpg");
	b_minus.addEventListener(MouseEvent.CLICK, onMinus);
	b_plus.addEventListener(MouseEvent.CLICK, onPlus);
}

init();